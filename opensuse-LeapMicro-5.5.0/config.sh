#!/bin/bash
# Copyright (c) 2018 SUSE LLC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
#======================================
# Functions...
#--------------------------------------

test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

set -euxo pipefail

mkdir /var/lib/misc/reconfig_system

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$kiwi_iname]-[$kiwi_profiles]..."

#======================================
# add missing fonts
#--------------------------------------
CONSOLE_FONT="eurlatgr.psfu"

#======================================
# prepare for setting root pw, timezone
#--------------------------------------
echo ** "reset machine settings"
sed -i 's/^root:[^:]*:/root:*:/' /etc/shadow
rm /etc/machine-id
rm /var/lib/zypp/AnonymousUniqueId

#======================================
# Setup baseproduct link
#--------------------------------------
suseSetupProduct

#======================================
# Specify default runlevel
#--------------------------------------
baseSetRunlevel 3

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey

#======================================
# If SELinux is installed, configure it like transactional-update setup-selinux
#--------------------------------------
if [[ -e /etc/selinux/config ]]; then
	# Check if we don't have selinux already enabled.
	grep ^GRUB_CMDLINE_LINUX_DEFAULT /etc/default/grub | grep -q security=selinux || \
	    sed -i -e 's|\(^GRUB_CMDLINE_LINUX_DEFAULT=.*\)"|\1 security=selinux selinux=1"|g' "/etc/default/grub"

	# Adjust selinux config
	sed -i -e 's|^SELINUX=.*|SELINUX=enforcing|g' \
	    -e 's|^SELINUXTYPE=.*|SELINUXTYPE=targeted|g' \
	    "/etc/selinux/config"

	# Move an /.autorelabel file from initial installation to writeable location
	test -f /.autorelabel && mv /.autorelabel /etc/selinux/.autorelabel
fi

systemctl enable NetworkManager

# Enable chrony
suseInsertService chronyd

#======================================
# Sysconfig Update
#--------------------------------------
echo '** Update sysconfig entries...'

echo FONT="$CONSOLE_FONT" >> /etc/vconsole.conf

# fix security level (boo#1171174)
sed -e '/^PERMISSION_SECURITY=s/easy/paranoid/' /etc/sysconfig/security
chkstat --set --system

#======================================
# SSL Certificates Configuration
#--------------------------------------
echo '** Rehashing SSL Certificates...'
update-ca-certificates

#======================================
# Import trusted rpm keys
#--------------------------------------
for i in /usr/lib/rpm/gnupg/keys/gpg-pubkey*asc; do
    # importing can fail if it already exists
    rpm --import $i || true
done

# Add repos from /etc/YaST2/control.xml
if [ -x /usr/sbin/add-yast-repos ]; then
	add-yast-repos
	zypper --non-interactive rm -u live-add-yast-repos
fi

# Adjust zypp conf
# https://github.com/openSUSE/libzypp/issues/212
# in yast that's done in packager/cfa/zypp_conf.rb
sed -i 's/.*solver.onlyRequires.*/solver.onlyRequires = true/g' /etc/zypp/zypp.conf
sed -i 's/.*rpm.install.excludedocs.*/rpm.install.excludedocs = yes/g' /etc/zypp/zypp.conf
sed -i 's/^multiversion =.*/multiversion =/g' /etc/zypp/zypp.conf

#=====================================
# Configure snapper
#-------------------------------------
if [ "${kiwi_btrfs_root_is_snapshot-false}" = 'true' ]; then
        echo "creating initial snapper config ..."
	cp /etc/snapper/config-templates/default /etc/snapper/configs/root
        baseUpdateSysConfig /etc/sysconfig/snapper SNAPPER_CONFIGS root

	# Adjust parameters
	sed -i'' 's/^TIMELINE_CREATE=.*$/TIMELINE_CREATE="no"/g' /etc/snapper/configs/root
	sed -i'' 's/^NUMBER_LIMIT=.*$/NUMBER_LIMIT="2-10"/g' /etc/snapper/configs/root
	sed -i'' 's/^NUMBER_LIMIT_IMPORTANT=.*$/NUMBER_LIMIT_IMPORTANT="4-10"/g' /etc/snapper/configs/root
fi

# The %post script can't edit /etc/fstab sys due to https://github.com/OSInside/kiwi/issues/945
# so use the kiwi custom hack
cat >/etc/fstab.script <<"EOF"
#!/bin/sh
set -eux

/usr/sbin/setup-fstab-for-overlayfs

# If /var is on a different partition than /...
if [ "$(findmnt -snT / -o SOURCE)" != "$(findmnt -snT /var -o SOURCE)" ]; then
	# ... set options for autoexpanding /var
	gawk -i inplace '$2 == "/var" { $4 = $4",x-growpart.grow,x-systemd.growfs" } { print $0 }' /etc/fstab
fi
EOF
chmod a+x /etc/fstab.script

# To make x-systemd.growfs work from inside the initrd
cat >/etc/dracut.conf.d/50-leap-micro-growfs.conf <<"EOF"
install_items+=" /usr/lib/systemd/systemd-growfs "
EOF

#======================================
# Boot TimeOut Configuration for iSCSI
#--------------------------------------
cat > /etc/systemd/system/iscsi-init-delay.service <<-EOF
[Unit]
# Workaround for boo#1198457 delay gen-initiatorname after local-fs
Description=One time delay for the iscsid.service
ConditionPathExists=!/etc/iscsi/initiatorname.iscsi
ConditionPathExists=/sbin/iscsi-gen-initiatorname
DefaultDependencies=no
RequiresMountsFor=/etc/iscsi
After=local-fs.target
Before=iscsi-init.service

[Install]
WantedBy=default.target

[Service]
Type=oneshot
RemainAfterExit=no
ExecStart=/sbin/iscsi-gen-initiatorname
EOF
ln -s /etc/systemd/system/iscsi-init-delay.service /etc/systemd/system/default.target.wants/iscsi-init-delay.service

#======================================
# User-defined settings
#--------------------------------------
echo "fs.inotify.max_user_watches = 1048576" >> /etc/sysctl.conf
echo "fs.inotify.max_user_instances = 1024" >> /etc/sysctl.conf

#==================================================
# Download RKE2 archives for airgapped installation
#--------------------------------------------------
mkdir /opt/rke2-artifacts
curl -sfL https://get.rke2.io > /opt/install.sh
export K8S_VERSION_WEB=${K8S_VERSION/-/%2B}
curl -sfL https://github.com/rancher/rke2/releases/download/v$K8S_VERSION_WEB/rke2-images.linux-amd64.tar.zst -o /opt/rke2-artifacts/rke2-images.linux-amd64.tar.zst
curl -sfL https://github.com/rancher/rke2/releases/download/v$K8S_VERSION_WEB/rke2.linux-amd64.tar.gz -o /opt/rke2-artifacts/rke2.linux-amd64.tar.gz
curl -sfL https://github.com/rancher/rke2/releases/download/v$K8S_VERSION_WEB/sha256sum-amd64.txt -o /opt/rke2-artifacts/sha256sum-amd64.txt

#======================================
# Install NMC
#--------------------------------------
mkdir /combustion
curl -o nmc -L https://github.com/suse-edge/nm-configurator/releases/latest/download/nmc-linux-$(uname -m)
chmod +x nmc
mv nmc /combustion/

#======================================
# Resize disk
#--------------------------------------
cat >/combustion/growfs <<"EOF"
#!/bin/bash
# Bugzilla - https://bugzilla.suse.com/show_bug.cgi?id=1217430
mnt="/"
dev="$(findmnt --fstab --target ${mnt} --evaluate --real --output SOURCE --noheadings)"
# /dev/sda3 -> /dev/sda, /dev/nvme0n1p3 -> /dev/nvme0n1
parent_dev="/dev/$(lsblk --nodeps -rno PKNAME "${dev}")"
# Last number in the device name: /dev/nvme0n1p42 -> 42
partnum="$(echo "${dev}" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/')"
ret=0
growpart "$parent_dev" "$partnum" || ret=$?
[ $ret -eq 0 ] || [ $ret -eq 1 ] || exit 1
/usr/lib/systemd/systemd-growfs "$mnt"
EOF
chmod +x /combustion/growfs

#========================================================================================================================================
# Add combustion script taken from https://github.com/suse-edge/metal3-demo/blob/main/roles/edge-image-builder/files/configure-network.sh
#----------------------------------------------------------------------------------------------------------------------------------------
cat >/combustion/script <<"EOF"
#!/bin/bash
# combustion: network prepare
set -euxo pipefail

nm_config() {
	CONFIG_DRIVE=$(blkid --label config-2 || true)
	if [ -z "${CONFIG_DRIVE}" ]; then
	  echo "No config-2 device found, skipping network configuration"
	  exit 0
	fi

	mount -o ro $CONFIG_DRIVE /mnt

	NETWORK_DATA_FILE="/mnt/openstack/latest/network_data.json"

	if [ ! -f "${NETWORK_DATA_FILE}" ]; then
	  umount /mnt
	  echo "No network_data.json found, skipping network configuration"
	  exit 0
	fi

	# FIXME: we can probably improve this, but there's no jq in the ramdisk
	DESIRED_HOSTNAME=$(cat /mnt/openstack/latest/meta_data.json | tr ',{}' '\n' | grep '\"metal3-name\"' | sed 's/.*\"metal3-name\": \"\(.*\)\"/\1/')

        # Remove the content of system-connections
        rm -rf /var/run/NetworkManager/system-connections/*

	mkdir -p /tmp/nmc/{desired,generated}
	cp ${NETWORK_DATA_FILE} /tmp/nmc/desired/${DESIRED_HOSTNAME}.yaml
	umount /mnt

	./nmc generate --config-dir /tmp/nmc/desired --output-dir /tmp/nmc/generated
	./nmc apply --config-dir /tmp/nmc/generated
}

if [ "${1-}" = "--prepare" ]; then
    nm_config # Configure NM in the initrd
    exit 0
fi
nm_config # Configure NM in the system
./growfs
EOF

#======================================
# Refresh zypper services / repos
#--------------------------------------

zypper refs

exit 0
