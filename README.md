# kiwi-imagebuilder

::: Note::: This project is under PoC and needs lot of improvements

## Creating OpenSuse Immutable Image

This document will help in creating OpenSuse Immutable image using Kiwi.
This will create image of devel Leap Micro from SUSE

## Prerequisites

- Create a VM (preferably Ubuntu with good disk size around 80GB, from where immutable image will be created)
- Kiwi
- Zypper

## Steps to create Immutable OS from OpenSuse Leap Micro OS

### Create Ubuntu VM (preferable 22.04)

    - Set Proxy:

        ```
        # Add required proxy to /etc/environment
        export http_proxy=
        export https_proxy=
        export no_proxy=
        source /etc/environment
        ```

    - Commands to set environment:

        ```
        apt update
        apt install alien rpm zypper python3-pip qemu-kvm virt-manager bridge-utils mtools -y
        pip install kiwi
        ```

### Get OpenSuse Repo

    ```
    git clone https://gitlab.com/sylva-projects/sylva-elements/kiwi-imagebuilder.git
    cd `kiwi-imagebuilder
    ```

### Create Image

    ```
    K8S_VERSION=1.28.12-rke2r1 kiwi-ng --debug --profile sylva  system build --description . --target-dir .
    ```

    Navigate to `target-dir` to find the created image.


### Deploy Sylva with newly created image

Deploy Sylva with newly create immutable OS.

### Changes required in existing Sylva to make it compatible with immutable OS

    Make changes in sylva-core and sylva-capi-cluster to deploy the management cluster using the created image.

- Changes in [sylva-capi-cluster](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/merge_requests/414/diffs)
- Changes in [sylva-core](https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/2659/diffs)
